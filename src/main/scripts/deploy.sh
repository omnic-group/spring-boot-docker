cd ../../../
docker stop spring-boot-docker && docker rm spring-boot-docker
docker rmi -f springio/spring-boot-docker:1.0
gradle clean build -x test -x check buildDocker
docker run -d -p 8081:8080 --name spring-boot-docker springio/spring-boot-docker:1.0