package com.bluecoffee.model;

/**
 * Created by qianlong on 2017/4/27.
 */
public class UserInfo implements java.io.Serializable {

    private static final long serialVersionUID = 6913373831037779112L;

    private String userName;
    private Integer age;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
