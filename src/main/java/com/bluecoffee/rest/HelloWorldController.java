package com.bluecoffee.rest;

import com.bluecoffee.model.UserInfo;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qianlong on 2017/4/27.
 */
@RestController
public class HelloWorldController {

    @RequestMapping(value = "/sayHello", method = RequestMethod.POST, produces = "application/json")
    public String sayHello(@RequestParam String username) {
        String str = "Hello," + username;
        return str;
    }

    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET, consumes = "application/json")
    public String getUser(@PathVariable String username, @RequestParam String pwd) {
        return "Welcome," + username+",your password is "+pwd;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET, consumes = "application/json")
    public List<UserInfo> getUserInfo(){
        List<UserInfo> userInfos = new ArrayList<UserInfo>();
        for(int i=0;i<5;i++){
            UserInfo user = new UserInfo();
            user.setUserName("user"+i);
            user.setAge(20+i);
            userInfos.add(user);
        }
        return userInfos;
    }

}
