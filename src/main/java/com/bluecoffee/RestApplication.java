package com.bluecoffee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by qianlong on 2017/4/27.
 */
@SpringBootApplication
public class RestApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(RestApplication.class, args);
    }
}
